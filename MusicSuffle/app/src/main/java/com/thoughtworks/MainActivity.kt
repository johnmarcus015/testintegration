package com.thoughtworks

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import android.widget.TextView
import com.thoughtworks.integrations.RetrofitConfig
import com.thoughtworks.models.Music
import com.thoughtworks.models.MusicResponse
import com.thoughtworks.models.Musics
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val responseView = findViewById<TextView>(R.id.responseView)
        val musics = null

        val request = RetrofitConfig().musicService().list(909253)
        request.enqueue(object : Callback<Musics?> {
            override fun onResponse(
                call: Call<Musics?>?,
                response: Response<Musics?>?
            ) {
                val response = response?.body()?.let {
                    configListViewMusics(it)
                }
            }

            override fun onFailure(
                call: Call<Musics?>?,
                t: Throwable?
            ) {
                responseView.text = t?.message
            }
        })
    }

    fun configListViewMusics(musics: Musics){
        val musicsName = arrayListOf<String>()
        for (x in 0..(musics.resultCount-1)){
            val music: Music = musics.results[x]
            musicsName.add(music.artistName)
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, musicsName)
        listViewMusics.adapter = adapter
    }
}
