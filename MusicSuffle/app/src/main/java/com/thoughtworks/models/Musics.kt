package com.thoughtworks.models

import com.google.gson.annotations.SerializedName

class Musics(
    @SerializedName("resultCount")
    val resultCount: Int,
    @SerializedName("results")
    val results: List<Music>
)
