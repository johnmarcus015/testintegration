package com.thoughtworks.integrations.services

import com.thoughtworks.models.MusicResponse
import com.thoughtworks.models.Musics
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MusicService {

    @GET("lookup")
    fun list(@Query("id") id: Int) : Call<Musics>

}
