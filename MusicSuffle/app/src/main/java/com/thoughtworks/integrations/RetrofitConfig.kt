package com.thoughtworks.integrations

import com.thoughtworks.integrations.services.MusicService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig {

    private val host = "https://us-central1-tw-exercicio-mobile.cloudfunctions.net/"
    private val converter = GsonConverterFactory.create()

    private val retrofit = Retrofit.Builder()
        .baseUrl(host)
        .addConverterFactory(converter)
        .build()

    fun musicService() = retrofit.create(MusicService::class.java)

}